# ScopeFun FPGA firmware sources

This is the [Xilinx Artix-7 FPGA](https://www.xilinx.com/products/silicon-devices/fpga/artix-7.html) firmware source code for ScopeFun.

## Getting started

For comiling the FPGA firmware you must install the [ISE WebPACK Design Software](https://www.xilinx.com/products/design-tools/ise-design-suite/ise-webpack.html) from Xilinx.

After completing Vivado installation run the "gen_project.tcl" script from Vivado (Tools -> Run Tcl Script). This will create a new project and link ScopeFun sources from "srcs" folder.

## Licensing

ScopeFun FGPA firmware sources are licensed under GNU General Public License v3 (GPLv3). For details please see the COPYING file(s) and file headers.